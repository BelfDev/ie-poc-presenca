"use strict";
import axios from "axios";
import Configs from "./../utils/Configs";
import PropTypes from "prop-types";

const apiService = axios.create({
  baseURL: Configs.BASE_URL,
  timeout: 3000,
  headers: { "Content-Type": "multipart/form-data" },
  params: {
    key: Configs.API_KEY,
    _session_id: Configs.SESSION_ID
  }
});

export const postAttendance = scannedData => {
  let bodyFormData = new FormData();
  bodyFormData.append("fields", JSON.stringify(scannedData));

  return apiService
    .post(Configs.ATTENDANCE_ENDPOINT + "create.json", bodyFormData)
    .then(response => response)
    .catch(error => error);
};

postAttendance.propTypes = {
  scannedData: PropTypes.object.isRequired
};

export default apiService;
