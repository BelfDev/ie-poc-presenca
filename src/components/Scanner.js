"use strict";
import React from "react";
import { View, StyleSheet } from "react-native";
import { RNCamera } from "react-native-camera";
import PropTypes from "prop-types";

const scanner = props => (
    <View style={styles.container}>
        <RNCamera
          style={styles.preview}
          type={RNCamera.Constants.Type.back}
          barCodeTypes={[RNCamera.Constants.BarCodeType.qr]}
          onBarCodeRead={props.onBarCodeRead}
        />
      </View>
);

scanner.propTypes = {
  onBarCodeRead: PropTypes.func.isRequired
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row"
  },
  preview: {
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "center"
  }
});

export default scanner;
