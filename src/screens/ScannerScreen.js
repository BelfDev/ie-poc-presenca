"use strict";
import React, { Component } from "react";
import { Alert, StyleSheet, Text, View } from "react-native";
import { RNCamera } from "react-native-camera";
import { isEqual } from "lodash";
import { postAttendance } from "./../networking/ApiService";
import Scanner from "./../components/Scanner";

export default class ScannerScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      qrcode: "",
      scanCounter: 0
    };
  }

  onBarCodeRead = e => {
    try {
      let studentInfo = JSON.parse(e.data);
      let shouldRegister = !isEqual(studentInfo, this.state.qrcode);

      if (shouldRegister) {
        this.setState({
          qrcode: studentInfo,
          scanCounter: 0
        });
        this.registerAttendance(studentInfo);
      } else {
        this.setState({
          scanCounter: this.state.scanCounter + 1
        });
        if (this.state.scanCounter === 1) {
          Alert.alert("Aluno já cadastrado!");
        }
      }
    } catch (error) {
      console.log(error);
    }
  };

  registerAttendance = studentInfo => {
    postAttendance(studentInfo)
      .then(response => {
        console.log(response);
        Alert.alert("Aluno registrado com sucesso:\n" + studentInfo.nome);
      })
      .catch(error => {
        console.error(error);
        Alert.alert("Falha registrar aluno...");
      });
  };

  render() {
    return (
      <View style={styles.container}>
        <Scanner onBarCodeRead={this.onBarCodeRead} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row"
  }
});

//TODO: Create registration dialog component
//TODO: Make a chain request for signing the user in a dummy account
//TODO: Validate if the QR Code is IE compatible
//TODO: Work on animations
//TODO: Test on older devices from both platforms
//TODO: Write unit tests
